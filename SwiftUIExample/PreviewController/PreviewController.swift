//
//  PreviewController.swift
//  QuickLookDemo
//
//  Created by Mac on 22/04/23.
//  Copyright © 2023 Vivek. All rights reserved.
//

import Foundation
import QuickLook

@objc(PreviewController)
class PreviewController: NSObject{
   
    @objc var dismisPreview: (() -> Void)?
    
    let previewController = QLPreviewController()
    lazy var previewItem = NSURL()
    
    @objc
    static let shared = PreviewController()
    
    @objc func withPreviewItem(item : NSURL){
        self.previewItem = item
        previewController.dataSource = self
        previewController.delegate = self
    }
    @objc func showItem(vc : UIViewController){
        vc.present(previewController, animated: true, completion: nil)
    }
    
}
extension PreviewController: QLPreviewControllerDataSource, QLPreviewControllerDelegate{
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.previewItem as QLPreviewItem
    }
    func previewControllerDidDismiss(_ controller: QLPreviewController){
        self.dismisPreview?()
    }
}


/*
 
 -(IBAction)displayLocalFile:(id)sender{
     NSURL *url = [self getPreviewItem:@"sample.jpg"];
     PreviewController *shared = [PreviewController shared];
     [shared withPreviewItemWithItem:url];
     UIViewController *tempView = [[UIViewController alloc]init];
     [tempView.view setBackgroundColor:[UIColor redColor]];
     [self.view addSubview:tempView.view];
     tempView.view.frame = CGRectMake(0, 0, 100, 100);
     [shared showItemWithVc:self];
     shared.dismisPreview = ^{
         [tempView.view removeFromSuperview];
     };
 }
 */
