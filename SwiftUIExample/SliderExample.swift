//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct SliderExample: View {
    @State var progress: Float = 0
    @State var quantity: Int = 0
    var body: some View {
        VStack {
//            Slider(value: $progress, from: 0.0, through: 100.0, by: 5.0)
//            HStack {
//                Image(systemName: "sun.min")
//                Slider(value: $progress, from: 0.0, through: 100.0, by: 5.0)
//                Image(systemName: "sun.max.fill")
//            }.padding()
            
            
          
            Stepper(value: $quantity, in: 0...10, label: { Text("Quantity \(quantity)")})
            
            Stepper("Quantity \(quantity)", value: $quantity, in: 0...10)
            
            Stepper(onIncrement: {
                self.quantity += 1
            }, onDecrement: {
                self.quantity -= 1
            }, label: { Text("Quantity \(quantity)") })

            
            Stepper(value: $quantity, in: 0...10, step: 2) {
                Text("Quantity \(quantity)")
            }
        }
    }
}

struct SliderExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SliderExample()
        }
    }
}

