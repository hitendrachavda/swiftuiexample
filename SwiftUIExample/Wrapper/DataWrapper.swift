//
//  DataWrapper.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//

import Foundation
import FMDB

final class DataWrapper: ObservableObject {
    
    private let db: FMDatabase
    @Published var users = [User]()
    
    init(fileName: String = "test") {
        // 1 - Get filePath of the SQLite file
        let fileURL = try! FileManager.default
            .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            .appendingPathComponent("\(fileName).sqlite")
        
        // 2 - Create FMDatabase from filePath
        let db = FMDatabase(url: fileURL)
        print("FilePath",fileURL)
        // 3 - Open connection to database
        guard db.open() else {
            fatalError("Unable to open database")
        }
        
        // 4 - Initial table creation
        do {
            try db.executeUpdate("create table if not exists users(username varchar(255) primary key, age integer, id integer)", values: nil)
            try db.executeUpdate("create table if not exists doctors(doctor_id integer, doctor_name varchar(255), degree varchar(255))", values: nil)
            try db.executeUpdate("create table if not exists visits(doctor_id integer, patient_name varchar(255), vdate varchar(255))", values: nil)
        } catch {
            fatalError("cannot execute query")
        }
        
        self.db = db
        self.getAllUser()
        if !UserDefaults.standard.bool(forKey: "isFirstTime"){
            self.fillDoctersData()
            self.filPatinetData()
            self.getJoinList()
            UserDefaults.standard.set(true, forKey: "isFirstTime")
        }
    }
    func fillDoctersData(){
        do {
            try db.executeUpdate("insert into doctors (doctor_id, doctor_name, degree) values (210, 'Dr. John Linga', 'MD')",values: nil)
            try db.executeUpdate("insert into doctors (doctor_id, doctor_name, degree) values (211, 'Dr. Peter Hall', 'MBBS')",values: nil)
            try db.executeUpdate("insert into doctors (doctor_id, doctor_name, degree) values (212, 'Dr. Ke Gee', 'MD')",values: nil)
            try db.executeUpdate("insert into doctors (doctor_id, doctor_name, degree) values (213, 'Dr. Pat Fay', 'MD')",values: nil)
        } catch {
            fatalError("cannot insert user: \(error)")
        }
    }
    
    func filPatinetData(){
        do {
            try db.executeUpdate("insert into visits (doctor_id, patient_name, vdate) values (210, 'Julia Nayer', '2013-10-15')",values: nil)
            try db.executeUpdate("insert into visits (doctor_id, patient_name, vdate) values (214, 'TJ Olson', 2013-10-14)",values: nil)
            try db.executeUpdate("insert into visits (doctor_id, patient_name, vdate) values (215, 'John Seo', 2013-10-15)",values: nil)
            try db.executeUpdate("insert into visits (doctor_id, patient_name, vdate) values (212, 'James Marlow', 2013-10-16)",values: nil)
            try db.executeUpdate("insert into visits (doctor_id, patient_name, vdate) values (212, 'Jason Mallin', 2013-10-12)",values: nil)
        } catch {
            fatalError("cannot insert user: \(error)")
        }
    }
    
    func getAllUser(){
        users = getAllUsers()
    }
    
    func getJoinList(){
        do {
            //let result = try db.executeQuery("SELECT doctors.doctor_id,doctors.doctor_name,visits.patient_name FROM doctors INNER JOIN visits ON doctors.doctor_id=visits.doctor_id WHERE doctors.degree='MD'", values: nil)
            let result = try db.executeQuery("select * from doctors", values: nil)
            while result.next() {
                print(result)
            }
        } catch  {
            fatalError("cannot execute query")
        }
        ;
    }
    
    func getAllUsers() -> [User] {
        var users = [User]()
        do {
            let result = try db.executeQuery("select username, age, id from users", values: nil)
            while result.next() {
                if let user = User(from: result) {
                    users.append(user)
                }
            }
            return users
        } catch {
            return users
        }
    }
    
    func insert(_ user: User) {
        do {
            try db.executeUpdate("insert into users (username, age, id) values (?, ?, ?)",
                values: [user.username, user.age, user.id]
            )
            users.append(user)
        } catch {
            fatalError("cannot insert user: \(error)")
        }
    }
    
    func delete(_ user: User) {
        do {
            try db.executeUpdate("DELETE FROM users WHERE age = ?",values: [user.age])
            if let index = users.firstIndex(of: user) {
                users.remove(at: index)
            }
        } catch {
            fatalError("cannot delete: \(error)")
        }
    }
    func deleteall() {
        do {
            try db.executeUpdate("DELETE FROM users", values: nil)
            users.removeAll()
        } catch {
            fatalError("cannot delete: \(error)")
        }
    }
    func updateUser(_ user: User, username: String) {
        do {
            let result = try db.executeQuery("UPDATE users SET username = ? WHERE id = ?", values: [username, user.id])
            while result.next() {
                if let user = User(from: result) {
                    users.append(user)
                }
            }
        } catch {
            fatalError("cannot delete: \(error)")
        }
    }
}
