//
//  User.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//

import Foundation
import FMDB
struct User: Hashable, Decodable, Identifiable{
    var id : Int = 0
    let username : String
    let age : Int
    
    init(username : String, age : Int){
        self.username = username
        self.age = age
    }
    init?(from result: FMResultSet) {
        if let username = result.string(forColumn: "username") {
            self.username = username
            self.age = Int(result.int(forColumn: "age"))
            self.id = Int(result.int(forColumn: "id"))
        } else {
            return nil
        }
    }
    private enum CodingKeys : String, CodingKey {
        case username = "first_name"
        case id = "id"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        username = try container.decode(String.self, forKey: .username)
        id = try container.decode(Int.self, forKey: .id)
        age = Int.random(in: 1..<100)
    }
}
