//
//  UserDetail.swift
//  SwiftUIExample
//
//  Created by Mac on 12/02/23.
//

import SwiftUI

struct UserDetail: View {
    @Environment(\.dismiss) private var dismiss
    var db : DataWrapper
    @State var firstName : String = ""
    @State var user : User
    var body: some View {
        VStack{
            TextField("Username", text: $firstName)
                .padding(5)
                .border(.secondary)
                .padding(10)
            Spacer()
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    db.updateUser(self.user,username: firstName)
                    dismiss()
                } label: {
                    Image("save")
                        .resizable()
                        .frame(width: 30, height: 30, alignment: .center)
                }

            }
        }
    }
}

struct UserDetail_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            UserDetail(db: DataWrapper(), firstName: "test", user: User(username: "", age: 1))
        }
    }
}


