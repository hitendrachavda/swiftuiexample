//
//  DatabaseExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//

import SwiftUI

struct DatabaseExample: View {
    
    @ObservedObject var db = DataWrapper()
    @State var isloading : Bool = false
    
    var body: some View {
        NavigationView {
            VStack{
                if isloading {
                    ProgressView()
                        .frame(height:40)
                        .frame(maxWidth:.infinity, alignment: .center)
                }
                List{
                    ForEach(db.users) { (user : User)  in
                        HStack{
                            NavigationLink(destination: UserDetail(db: db, firstName: user.username,user: user)){
                                  Text(user.username)
                                  Text("\(user.age)")
                            }
                        }
                    }.onDelete { indexSet in
                        for index in indexSet{
                            db.delete(db.users[index])
                        }
                    }
                }
                .listStyle(GroupedListStyle())
                .padding(0)
            }
            .navigationTitle("Users")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(id: "plus", placement: .navigationBarTrailing, showsByDefault: true) {
                    Button(action: {
                        isloading = true
                        createRandomUser()
                    }, label: {
                        Image(systemName: "plus")
                            .frame(width: 25, height: 25, alignment: .leading)
                         .foregroundColor(.black)
                    })
                }
                ToolbarItem(id: "delete", placement: .navigationBarLeading, showsByDefault: true) {
                    Button(action: {
                        isloading = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            isloading = false
                            db.deleteall()
                        }
                    }, label: {
                        Image("delete")
                            .frame(width: 25, height: 25, alignment: .leading)
                            .foregroundColor(.black)
                    })
                }
            }
        }

        .onAppear(perform: {
                UITableView.appearance().contentInset.top = -35
                UITableView.appearance().contentInset.bottom = -35
        })
    }
    
    private func createRandomUser() {
        let url = URL(string: "https://random-data-api.com/api/name/random_name")!
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                fatalError("No data")
            }
            
            DispatchQueue.main.async {
                let user = try! JSONDecoder().decode(User.self, from: data)
                isloading = false
                db.insert(user)
            }
        }
        task.resume()
    }
}

struct DatabaseExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            DatabaseExample(db: DataWrapper())
        }
    }
}

