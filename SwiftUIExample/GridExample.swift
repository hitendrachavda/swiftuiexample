//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct GridExample: View {
    
    var rows: [GridItem] =
            Array(repeating: .init(.fixed(20)), count: 2)
    
    var columns: [GridItem] =
            Array(repeating: .init(.fixed(20)), count: 5)
    
    var body: some View {
        VStack(spacing:0){
            ScrollView(.horizontal) {
                LazyHGrid(rows: rows, alignment: .top,spacing: 10) {
                    ForEach((0...100), id: \.self) {
                        Text("\($0)")
                            .frame(width: 50, height: 50, alignment: .center)
                            .background(Color.pink)
                            
                    }
                }
            }
            
            ScrollView {
                LazyVGrid(columns: columns) {
                    ForEach((0...100), id: \.self) {
                        Text("\($0)").background(Color.pink)
                    }
                }
            }
        }
    }
}

struct GridExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            GridExample()
        }
    }
}

