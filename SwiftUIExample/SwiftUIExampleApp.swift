//
//  SwiftUIExampleApp.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//

import SwiftUI

@main
struct SwiftUIExampleApp: App {
    var body: some Scene {
        WindowGroup {
            //TextandLabel()
            // TextFieldExample()
            //ImageExample()
            //ButtonExample()
            //NavigationExample()
            //ToolBarExample()
            //DatabaseExample()
            //DatePickerExample()
            //ProgressViewExample()
            //SliderExample()
            //StackExample()
            //ZstackExample()
            //ListExample()
            //GridExample()
            //SpacerExample()
            DatabaseExample()
//            StateObjectView()
        }
    }
}
