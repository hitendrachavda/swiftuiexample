//
//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct ButtonExample: View {
    var body: some View {
        VStack(spacing:10){
            //A control that performs an action when triggered.
            Button(
                action: {
                    // did tap
                },
                label: { Text("Click Me") }
            )
            Button("Click Me") {
                // did tap
            }
            Button(action: {
                            
            }, label: {
                Image(systemName: "clock")
                Text("Click Me")
                Text("Subtitle")
            })
            .foregroundColor(Color.white)
            .padding()
            .background(Color.blue)
            .cornerRadius(5)

            //Create a link-style Button that will open in the associated app, if possible, but otherwise in the user’s default web browser.
            

            Link("View Our Terms of Service", destination: URL(string: "https://www.example.com/TOS.html")!)

            Spacer()
            
        }
    }
}

struct ButtonExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ButtonExample()
        }
    }
}

