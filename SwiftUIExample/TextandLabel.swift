//
//  ContentView.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//

import SwiftUI

struct TextandLabel: View {
    
    struct VerticalLabelStyle: LabelStyle {
        func makeBody(configuration: Configuration) -> some View {
            HStack {
                configuration.title
                configuration.icon
            }
        }
    }
    
    
    struct YellowBackgroundLabelStyle: LabelStyle {
        func makeBody(configuration: Configuration) -> some View {
            Label(configuration)
                .padding()
                .background(Color.yellow)
        }
    }
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }()
    var now = Date()
    var body: some View {
        VStack(spacing:10){
            // A view that displays one or more lines of read-only text.
            
            Text("What time is it?:\(now, formatter: Self.dateFormatter)")
                .font(.system(size: 20,weight:.bold))
                .italic()
                .underline()
                .padding()
                .lineLimit(2)
            Text("Hello ") + Text("World!").bold()
            Text("Hello\nWorld!")
                .multilineTextAlignment(.center)
            
            //Label is a convenient view that presents an image and text alongside each other. This is suitable for a menu item or your settings.
            Label("Swift1", image: "swift")
                .font(.system(size: 20))
            Label {
                Text("SwiftUI")
                    .foregroundColor(Color.red)
            } icon: {
                Image(systemName: "keyboard")
                    .foregroundColor(Color.blue)
            }

            Label("SwiftUI Tutorials", systemImage: "book.fill")
                .labelStyle(VerticalLabelStyle())
            
            
            Label("SwiftUI Tutorials", systemImage: "book.fill")
                .labelStyle(YellowBackgroundLabelStyle())
            
            Spacer()
        }
    }
}

struct TextandLabel_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TextandLabel()
        }
    }
}
