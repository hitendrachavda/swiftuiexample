
//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct ToolBarExample: View {
    
    @State var isShowing = true // toggle state

    var body: some View {
        NavigationView {
            VStack{
                Text("SwiftUI").padding()
                    .background(.red)
                    .toolbar {
                        ToolbarItem(placement: .primaryAction) {
                            Button {
                                
                            } label: {
                                Image(systemName: "square.and.pencil")
                            }

                        }
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button {
                                
                            } label: {
                                Image(systemName: "square.and.pencil")
                            }

                        }
                    }
                Toggle(isOn: $isShowing) {
                    Text("Hello World")
                }
                .background(.red)
            }
            
        }
    }
}

struct ToolBarExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ToolBarExample()
        }
    }
}

