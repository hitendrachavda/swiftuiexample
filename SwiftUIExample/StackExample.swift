//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct StackExample: View {
    var body: some View {
        //A view that arranges its children in a horizontal line.
        VStack(spacing:0){
            HStack (alignment: .center, spacing: 20){
                Text("Hello")
                Divider()
                Text("World")
            }
            
            //A view that arranges its children in a line that grows horizontally, creating items only as needed.
            ScrollView(.horizontal) {
                LazyHStack(alignment: .center, spacing: 20) {
                    ForEach(1...100, id: \.self) {
                        Text("Column \($0)")
                    }
                }
            }
            
            VStack (alignment: .center, spacing: 20){
                Text("Hello")
                Divider()
                Text("World")
            }
            ScrollView {
                LazyVStack(alignment: .leading) {
                    ForEach(1...100, id: \.self) {
                        Text("Row \($0)")
                    }
                }
            }
            Spacer()
        }
        
    }
}

struct StackExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            StackExample()
        }
    }
}

