
//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct NavigationExample: View {
    var body: some View {
        VStack(spacing:10){
            NavigationView {
                NavigationLink(destination: ButtonExample()) {
                    Text("Push")
                }.navigationBarTitle(Text("Master"))
            }
        }
    }
}

struct NavigationExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ButtonExample()
        }
    }
}

