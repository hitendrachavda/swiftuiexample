//
//  TextField.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//

import SwiftUI

struct TextFieldExample: View {
    enum FocusedField {
        case firstName, lastName
    }
    
    @State private var firstName = ""
    @State private var lastName = ""
    @State var password: String = "1234"

    
    @FocusState private var focusedField: FocusedField?
    @State private var fullText: String = "This is some editable text...This is some editable text...This is some editable text...This is some editable text...This is some editable text...This is some editable text...This is some editable text...This is some editable text..."

    
    var body: some View {
        VStack{
            //A control that displays an editable text interface.
            TextField("First name", text: $firstName)
                .focused($focusedField, equals: .firstName)
            TextField("Last name", text: $lastName)
                .focused($focusedField, equals: .lastName)
                .textInputAutocapitalization(.never)
                .padding(.vertical,10)
                .border(.secondary)
                .disableAutocorrection(true)
            
            //A control into which the user securely enters private text.
            SecureField("string", text: $password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding()
            
            //A view that can display and edit long-form text.
            
            TextEditor(text: $fullText)
                .frame(height: 100)
            Spacer()
            
        }
        .onAppear {
            focusedField = .firstName
        }
    }
}

struct TextFieldExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TextFieldExample()
        }
    }
}
