//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct SpacerExample: View {
    var body: some View {
        VStack(spacing:0){
            HStack {
                Image(systemName: "clock")
                Spacer()
                Text("Time")
            }
            HStack {
                Image(systemName: "clock")
                Divider()
                Text("Time")
            }.fixedSize()
            
            Spacer()
        }
    }
}

struct SpacerExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SpacerExample()
        }
    }
}

