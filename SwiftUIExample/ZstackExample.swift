//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct ZstackExample: View {
    var body: some View {
        ZStack {
            Color.ui.yellow.edgesIgnoringSafeArea(.all)
            Text("Hello")
                .padding(10)
                .background(Color.red)
                .opacity(0.8)
            Text("World")
                .padding(20)
                .background(Color.red)
                .offset(x: 0, y: 30)
        }
    }
}

struct ZstackExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ZstackExample()
        }
    }
}

