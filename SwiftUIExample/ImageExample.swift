//
//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

extension Color {
    static let ui = Color.UI()
    
    struct UI {
         let yellow = Color("skyblue")
    }
}

struct ImageExample: View {
    var body: some View {
        VStack{
            Image("swift2")
                .resizable()
                .frame(width: 100, height: 100, alignment: .center)
            Image(systemName: "clock.fill")
                .frame(width: 100, height: 100, alignment: .center)
            
            Image(systemName: "cloud.heavyrain.fill")
                .foregroundColor(Color.ui.yellow)
                .font(.title)
            
            Image(systemName: "clock")
                .foregroundColor(.red)
                .font(Font.system(.largeTitle).bold())
            Image("swift2")
                .resizable() // it will sized so that it fills all the available space
                .aspectRatio(contentMode: .fit)

            Spacer()
            
        }
    }
}

struct ImageExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ImageExample()
        }
    }
}

