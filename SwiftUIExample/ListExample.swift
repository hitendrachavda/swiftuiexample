//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct ListExample: View {
    let names = ["John", "Apple", "Seed"]
    var body: some View {
        VStack{
            // Simple List 1
            List {
                Text("Hello world")
                Text("Hello world")
                Text("Hello world")
            }
            
            //Cell can be mixed  2 Example
            List {
                Text("Hello world")
                Image(systemName: "clock")
            }
            
            List(names, id: \.self) { name in
                Text(name)
            }
            
            List {
                Section(header: Text("UIKit"), footer: Text("We will miss you")) {
                    Text("UITableView")
                }

                Section(header: Text("SwiftUI"), footer: Text("A lot to learn")) {
                    Text("List")
                }
            }
            
            
            List {
                Section(header: Text("UIKit"), footer: Text("We will miss you")) {
                    Text("UITableView")
                }

                Section(header: Text("SwiftUI"), footer: Text("A lot to learn")) {
                    Text("List")
                }
            }.listStyle(GroupedListStyle())
        }
        
    }
}

struct ListExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ListExample()
        }
    }
}

