//  ImageExample.swift
//  SwiftUIExample
//
//  Created by Mac on 11/02/23.
//
import SwiftUI

struct ProgressViewExample: View {
    @State private var progress = 0.05

    var body: some View {
        VStack {
            ProgressView(value: progress)
                .progressViewStyle(CircularProgressViewStyle())
            Spacer()
                .frame(height: 50)
            ProgressView(value: progress)
            Button("More", action: { progress += 0.05 })
        }
    }
}

struct ProgressViewExample_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ProgressViewExample()
        }
    }
}

