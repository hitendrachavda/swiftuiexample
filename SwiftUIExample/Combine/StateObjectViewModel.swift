//
//  StateObjectViewModel.swift
//  SwiftUIExample
//
//  Created by Mac on 15/02/23.
//

import Foundation
import Combine

class StateObjectViewModel:ObservableObject{
    
    @Published  var operatingSystem:String = "iOS"
    @Published  var age:Int = 30
    
    var cancellable=Set<AnyCancellable>()
    
    func changeAge()  {
        Just(30)
            .map { input in
                return input * 13
            }
            .sink { value in
                self.age=value
            }
            .store(in: &cancellable)
    }
    
    func changeOperatinSystem()  {
        Just("Android")
            .map { input in
                return input.uppercased()
            }
            .sink { value in
                self.operatingSystem=value
            }
            .store(in: &cancellable)
    }
}
