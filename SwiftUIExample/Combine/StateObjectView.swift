//
//  StateObjectView.swift
//  SwiftUIExample
//
//  Created by Mac on 15/02/23.
//

import SwiftUI

struct StateObjectView: View {
    
    @StateObject private var stateObjectViewModel=StateObjectViewModel()
    
    var body: some View {
        
        VStack(spacing:25) {
            Text(stateObjectViewModel.operatingSystem)
                .font(.title)
            .fontWeight(.medium)
            
            Text("\(stateObjectViewModel.age)")
                .font(.title3)
            .fontWeight(.light)
            
            Button {
                stateObjectViewModel.changeAge()
            } label: {
                Text("Change Age")
            }
            
            Button {
                stateObjectViewModel.changeOperatinSystem()
            } label: {
                Text("Change Operatin System")
            }

        }
        
    }
}

struct StateObjectView_Previews: PreviewProvider {
    static var previews: some View {
        StateObjectView()
    }
}
