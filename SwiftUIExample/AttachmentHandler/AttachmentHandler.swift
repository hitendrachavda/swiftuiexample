//
//  AttachmentHandler.swift
//  UnZipExample
//
//  Created by Hitendra.Chavda on 01/03/23.
//

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos

@objc(AttachmentHandler)
class AttachmentHandler: NSObject, UIPopoverPresentationControllerDelegate{
    @objc
    static let shared = AttachmentHandler()
    fileprivate var currentVC: UIViewController?
    //MARK: - Internal Properties
    @objc var imagePickedBlock: ((UIImage,String) -> Void)?
    @objc var videoPickedBlock: ((NSURL) -> Void)?
    @objc var filePickedBlock: (([URL]) -> Void)?
    @objc var didselectOption: ((AttachmentType) -> Void)?
    @objc var dimissPopover: (() -> Void)?


    
    var options : [Options] = []
    
    @objc enum AttachmentType: Int{
        case camera
        case photoLibrary
        case video
        case file
        
        func name() -> String {
            switch self {
            case .camera: return "Camera"
            case .photoLibrary: return "Photo Library"
            case .video: return "Video"
            case .file: return "File"
            }
        }
    }
    
    
   @objc enum Options : Int{
        case camera
        case phoneLibrary
        case video
        case file
        
        func name() -> String {
            switch self {
            case .camera: return "Camera"
            case .phoneLibrary: return "Phone Library"
            case .video: return "Video"
            case .file: return "File"
            }
        }
    }
    
    //MARK: - Constants
    struct Constants {
        static let actionFileTypeHeading = "Add a File"
        static let actionFileTypeDescription = "Choose a filetype to add..."
    
        static let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
        
        static let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
        
        static let alertForVideoLibraryMessage = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access."
        
        
        static let settingsBtnTitle = "Settings"
        static let cancelBtnTitle = "Cancel"
        
    }
    
    
    //MARK: - showAttachmentActionSheet
    // This function is used to show the attachment sheet for image, video, photo and file.
    @objc func showAttachmentActionSheet(vc: UIViewController, values:[Int], sourceView : UIButton?) {
        
        self.options.removeAll()
        for index in values {
            self.options.append(Options(rawValue: index)!)
        }
        currentVC = vc
        let height = self.options.count * 44
        let controller = UIViewController()
        let tableivew = UITableView()
        tableivew.isScrollEnabled  = false
        tableivew.separatorStyle = .none
        controller.view.addSubview(tableivew)
        tableivew.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "CustomCell")

        tableivew.frame = CGRect(x: 0, y: 0, width: 150, height: height)
        tableivew.delegate  = self
        tableivew.dataSource = self
        controller.preferredContentSize = CGSize(width: 150, height: height + 5)
        controller.modalPresentationStyle = .popover
        if let popover = controller.popoverPresentationController  {
            popover.delegate = self
            popover.sourceView = vc.view
            popover.backgroundColor = .white
                popover.sourceRect = CGRect(x: sourceView!.frame.origin.x, y: sourceView!.frame.origin.y, width: 0, height: 0)
            popover.permittedArrowDirections = [.down]
            currentVC!.present(controller, animated: true, completion: nil)
        }
        /*let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        if(options.contains{$0 == .camera}){
            actionSheet.addAction(UIAlertAction(title: Options.camera.name(), style: .default, handler: { (action) -> Void in
                self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
            }))
        }
        if(options.contains{$0 == .phoneLibrary}){
            actionSheet.addAction(UIAlertAction(title: Options.phoneLibrary.name(), style: .default, handler: { (action) -> Void in
                self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
            }))
        }
        if(options.contains{$0 == .video}){
            actionSheet.addAction(UIAlertAction(title: Options.video.name(), style: .default, handler: { (action) -> Void in
                self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
                
            }))
        }
        if(options.contains{$0 == .file}){
            actionSheet.addAction(UIAlertAction(title: Options.file.name(), style: .default, handler: { (action) -> Void in
                self.documentPicker()
            }))
        }
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        if let presenter = actionSheet.popoverPresentationController {
            if(sourceView != nil){
                presenter.sourceView = sourceView
                presenter.sourceRect = CGRect(x: sourceView!.bounds.midX, y: sourceView!.frame.origin.y, width: 0, height: 0)
                presenter.permittedArrowDirections = [.any]
            }
            else{
                presenter.sourceView = currentVC!.view
                presenter.sourceRect = CGRect(x: currentVC!.view.bounds.midX, y: currentVC!.view.frame.size.height/2, width: 0, height: 0)
                presenter.permittedArrowDirections = []
            }
        }
        vc.present(actionSheet, animated: true, completion: nil)*/
    }
    
    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library, video.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        self.dimissPopover?()
       }
    @objc func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
        currentVC = vc
        if attachmentTypeEnum == AttachmentType.file{
            self.documentPicker()
            return
        }
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                photoLibrary()
            }
            if attachmentTypeEnum == AttachmentType.video{
                videoLibrary()
            }
        case .denied:
            print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    DispatchQueue.main.async {
                        if attachmentTypeEnum == AttachmentType.camera{
                            self.openCamera()
                        }
                        if attachmentTypeEnum == AttachmentType.photoLibrary{
                            self.photoLibrary()
                        }
                        if attachmentTypeEnum == AttachmentType.video{
                            self.videoLibrary()
                        }
                    }
                }else{
                    print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    
    //MARK: - CAMERA PICKER
    //This function is used to open camera from the iphone and
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    

    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - FILE PICKER
    func documentPicker(){
        let importMenu = UIDocumentPickerViewController(documentTypes: ["com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document","org.openxmlformats.spreadsheetml.sheet", kUTTypePDF as String], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        if #available(iOS 11.0, *) {
            importMenu.allowsMultipleSelection = true
        }
        currentVC?.present(importMenu, animated: true, completion: nil)
    }
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = Constants.alertForVideoLibraryMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
}

//MARK: - IMAGE PICKER DELEGATE
// This is responsible for image picker interface to access image, video and then responsibel for canceling the picker
extension AttachmentHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
        self.dimissPopover?()
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            var imageName : String = ""
            if let phAsset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
                imageName = ""
                let resources = PHAssetResource.assetResources(for: phAsset)
                imageName = resources[0].originalFilename
            }
            print(imageName)
            self.imagePickedBlock?(image,imageName)
        } else{
            print("Something went wrong in  image")
        }
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL{
            print("videourl: ", videoUrl)
            //trying compression of video
            let data = NSData(contentsOf: videoUrl as URL)!
            print("File size before compression: \(Double(data.length / 1048576)) mb")
            compressWithSessionStatusFunc(videoUrl)
        }
        else{
            print("Something went wrong in  video")
        }
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Video Compressing technique
    fileprivate func compressWithSessionStatusFunc(_ videoUrl: NSURL) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                
                DispatchQueue.main.async {
                    self.videoPickedBlock?(compressedURL as NSURL)
                }
                
            case .failed:
                break
            case .cancelled:
                break
            @unknown default:
                fatalError()
            }
        }
    }
    
    // Now compression is happening with medium quality, we can change when ever it is needed
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
}

//MARK: - FILE IMPORT DELEGATE
extension AttachmentHandler: UIDocumentPickerDelegate{

    //    Method to handle cancel action.
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        // do something with the selected documents
        if urls.count > 0 {
            self.filePickedBlock?(urls)
        }
    }
}

extension AttachmentHandler{
    
    @objc func documentDirURL()-> URL{
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return documentDirURL

    }
    @objc func documentDirURLWithFolder(foldername : String)-> URL{
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let directoryURL = documentDirURL.appendingPathComponent(foldername, isDirectory: true)
        do {
            try FileManager.default.createDirectory(at: directoryURL, withIntermediateDirectories: true, attributes: nil)
            return directoryURL
        } catch {
            print(error.localizedDescription)
        }
        return directoryURL
        
    }
    
    @objc func saveImage(filename : String?, foldername : String?, filePathExtension : NSString, data : NSData){
        let fileURL = foldername != nil ?
        documentDirURLWithFolder(foldername: foldername!).appendingPathComponent(filename!).appendingPathExtension("\(filePathExtension)")
        :
        documentDirURL().appendingPathComponent(filename!).appendingPathExtension("\(filePathExtension)")
        do {
            try data.write(to: fileURL, options: .atomic)
        } catch {
            print("Unable to save in local", error)
            return
        }
        
    }
    
    @objc func getDirectoryFiles(folderName : String) -> [URL]{
        let fileManager = FileManager.default
        var documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        documentsURL = documentsURL.appendingPathComponent(folderName, isDirectory: true)
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            return fileURLs
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        return []
    }
    
    @objc func saveFile(url : URL, filename : String?, foldername : String?){
        let filePathExtension = url.pathExtension
        var fname = filename
        if(fname == nil){
            fname = (url.absoluteString as NSString).lastPathComponent
            fname = fname?.replacingOccurrences(of: ".\(filePathExtension)", with: "")
        }
        let fileURL = foldername != nil ?
        documentDirURLWithFolder(foldername: foldername!).appendingPathComponent(fname!).appendingPathExtension("\(filePathExtension)")
        :
        documentDirURL().appendingPathComponent(fname!).appendingPathExtension("\(filePathExtension)")
        do {
            let filedata = try Data(contentsOf: url as URL)
            print(filedata.base64EncodedString())
            do {
                try filedata.write(to: fileURL, options: .atomic)
            } catch {
                print("Unable to save in local", error)
                return
            }
        } catch {
            print("Unable to load data: \(error)")
        }
    }
    @objc func fileExistsAtPath(filePath : URL) -> Bool{
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath.path) {
            return true
        } else {
            return false
        }
    }

}
extension String {
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func base64Decoded() -> String? {
        var st = self;
        if (self.count % 4 <= 2){
            st += String(repeating: "=", count: (self.count % 4))
        }
        guard let data = Data(base64Encoded: st) else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
extension AttachmentHandler: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? CustomCell
        cell!.selectionStyle = .none
        cell!.viewseperator.isHidden = false
        if(self.options.count - 1  == indexPath.row){
            cell!.viewseperator.isHidden = true
        }
        let option = self.options[indexPath.row]
        cell!.lblname!.text = option.name()
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option = self.options[indexPath.row]
        var optionType = AttachmentType.camera
        if(option == .camera){
            optionType = .camera
           // self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }
        if(option == .file){
            optionType = .file
           // self.documentPicker()
            
        }
        if(option == .phoneLibrary){
            optionType = .photoLibrary
            //self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
        }
        self.didselectOption?(optionType)
        self.currentVC!.dismiss(animated: true)
    }
    
}

/*
 -(IBAction)sendershowAttachmentHandlar:(id)sender{
     NSArray *array = [[NSArray alloc] initWithObjects:@(OptionsFile),@(OptionsCamera),@(OptionsPhoneLibrary), nil];
     AttachmentHandler *shared = [AttachmentHandler shared];
     [shared showAttachmentActionSheetWithVc:self values:array sourceView:(UIButton *)sender];
     UIViewController *tempView = [[UIViewController alloc]init];
     [tempView.view setBackgroundColor:[UIColor redColor]];
     [self.view addSubview:tempView.view];
     shared.didselectOption = ^(AttachmentType type) {
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
             [[AttachmentHandler shared] authorisationStatusWithAttachmentTypeEnum:type vc:tempView];
         });
        
     };
     shared.dimissPopover = ^{
         [tempView.view removeFromSuperview];
     };
     

 }

 */
