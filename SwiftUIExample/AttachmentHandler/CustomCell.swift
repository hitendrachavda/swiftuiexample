//
//  CustomCell.swift
//  FileAttachmentExample
//
//  Created by Mac on 16/04/23.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var lblname : UILabel!
    @IBOutlet weak var viewseperator : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
